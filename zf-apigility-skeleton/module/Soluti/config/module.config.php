<?php
return [
    'router' => [
        'routes' => [
            'soluti.rest.certificados' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/certificados[/:certificados_id]',
                    'defaults' => [
                        'controller' => 'Soluti\\V1\\Rest\\Certificados\\Controller',
                    ],
                ],
            ],
        ],
    ],
    'zf-versioning' => [
        'uri' => [
            0 => 'soluti.rest.certificados',
        ],
    ],
    'zf-rest' => [
        'Soluti\\V1\\Rest\\Certificados\\Controller' => [
            'listener' => 'Soluti\\V1\\Rest\\Certificados\\CertificadosResource',
            'route_name' => 'soluti.rest.certificados',
            'route_identifier_name' => 'certificados_id',
            'collection_name' => 'certificados',
            'entity_http_methods' => [
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ],
            'collection_http_methods' => [
                0 => 'GET',
                1 => 'POST',
            ],
            'collection_query_whitelist' => [],
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => \Soluti\V1\Rest\Certificados\CertificadosEntity::class,
            'collection_class' => \Soluti\V1\Rest\Certificados\CertificadosCollection::class,
            'service_name' => 'certificados',
        ],
    ],
    'zf-content-negotiation' => [
        'controllers' => [
            'Soluti\\V1\\Rest\\Certificados\\Controller' => 'Json',
        ],
        'accept_whitelist' => [
            'Soluti\\V1\\Rest\\Certificados\\Controller' => [
                0 => 'application/json',
            ],
        ],
        'content_type_whitelist' => [
            'Soluti\\V1\\Rest\\Certificados\\Controller' => [
                0 => 'application/json',
            ],
        ],
    ],
    'zf-hal' => [
        'metadata_map' => [
            \Soluti\V1\Rest\Certificados\CertificadosEntity::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'soluti.rest.certificados',
                'route_identifier_name' => 'certificados_id',
                'hydrator' => \Zend\Hydrator\ArraySerializable::class,
            ],
            \Soluti\V1\Rest\Certificados\CertificadosCollection::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'soluti.rest.certificados',
                'route_identifier_name' => 'certificados_id',
                'is_collection' => true,
            ],
        ],
    ],
    'zf-apigility' => [
        'db-connected' => [
            'Soluti\\V1\\Rest\\Certificados\\CertificadosResource' => [
                'adapter_name' => 'Mysql',
                'table_name' => 'certificados',
                'hydrator_name' => \Zend\Hydrator\ArraySerializable::class,
                'controller_service_name' => 'Soluti\\V1\\Rest\\Certificados\\Controller',
                'entity_identifier_name' => 'id',
                'table_service' => 'Soluti\\V1\\Rest\\Certificados\\CertificadosResource\\Table',
            ],
        ],
    ],
    'zf-content-validation' => [
        'Soluti\\V1\\Rest\\Certificados\\Controller' => [
            'input_filter' => 'Soluti\\V1\\Rest\\Certificados\\Validator',
        ],
    ],
    'input_filter_specs' => [
        'Soluti\\V1\\Rest\\Certificados\\Validator' => [
            0 => [
                'name' => 'nome',
                'required' => true,
                'filters' => [
                    0 => [
                        'name' => \Zend\Filter\StringTrim::class,
                    ],
                    1 => [
                        'name' => \Zend\Filter\StripTags::class,
                    ],
                ],
                'validators' => [
                    0 => [
                        'name' => \Zend\Validator\StringLength::class,
                        'options' => [
                            'min' => 1,
                            'max' => '255',
                        ],
                    ],
                ],
            ],
            1 => [
                'name' => 'certificado',
                'required' => true,
                'filters' => [
                    0 => [
                        'name' => \Zend\Filter\StringTrim::class,
                    ],
                    1 => [
                        'name' => \Zend\Filter\StripTags::class,
                    ],
                ],
                'validators' => [
                    0 => [
                        'name' => \Zend\Validator\StringLength::class,
                        'options' => [
                            'min' => 1,
                            'max' => '65535',
                        ],
                    ],
                ],
            ],
        ],
    ],
];
